from configparser import ConfigParser
import uuid
from random import choice
from pyrogram import (
    Client,
    Filters,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    InlineQueryResultArticle,
    InputTextMessageContent,
)
from db_models import Argument, Counterargument

config = ConfigParser()
config.read("config.ini")
bot = Client("entkr", bot_token=config["other"]["bot_token"])
callback_data = {}


def make_callback_data(data):
    global callback_data
    uid = str(uuid.uuid4())
    callback_data[uid] = data
    return uid


def get_callback_data(uid):
    global callback_data
    return callback_data[uid]


@bot.on_message(Filters.command("start"))
def start(client, message):
    message.reply_text(config['strings']['start'])


@bot.on_message(Filters.command("help"))
def start(client, message):
    message.reply_text(config['strings']['help'])


@bot.on_message(Filters.command(["add_argument", "aa"]))
def add_argument(client, message):
    argument = " ".join(message.command[1:])
    uid = str(uuid.uuid4())
    message.reply_text(
        "Dankeschön für deinen Vorschlag, unser Team wird sich das jetzt angucken."
    )
    keyboard = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "Argument Hinzufügen",
                    callback_data=make_callback_data(f"aa|{argument}"),
                )
            ],
            [
                InlineKeyboardButton(
                    "Argument verwerfen", callback_data=make_callback_data("daa")
                )
            ],
        ]
    )
    client.send_message(
        chat_id=config["other"]["chat_id"],
        text=f"Argument: `{argument}`",
        reply_markup=keyboard,
    )


@bot.on_message(Filters.command(["add_counterargument", "aca"]))
def add_counterargument(client, message):
    counterargument = " ".join(message.command[1:])
    buttons = []
    for argument in Argument.select():
        buttons.append(
            [
                InlineKeyboardButton(
                    argument.argument,
                    callback_data=make_callback_data(
                        f"mca|{counterargument}|{argument.argument}"
                    ),
                )
            ]
        )
    message.reply_text(
        f"Zu welchem Argument ist `{counterargument}` ein gegen argument?",
        reply_markup=InlineKeyboardMarkup(buttons),
    )


@bot.on_callback_query()
def handle_callback_query(client, callback_query):
    data = get_callback_data(callback_query.data).split("|")
    # add argument
    if data[0] == "aa":
        Argument.create(argument=data[1])
        callback_query.message.edit(f"Argument `{data[1]}` hinzugefügt")
        callback_query.answer("Dankeschön, Argument wurde zur Datenbank hinzugefügt")
    # don't add argument
    elif data[0] == "daa":
        callback_query.message.delete()
        callback_query.answer(
            "Dankeschön, Argument wurde nicht zu Datenbank hinzugefügt"
        )
    # moderate counterargument
    elif data[0] == "mca":
        callback_query.message.edit(f"{callback_query.message.text} \n {data[2]}")
        callback_query.answer(
            "Dankeschön, das wird jetzt zu unserem Team weitergeleitet"
        )
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        "Gegenargument hinzufügen",
                        callback_data=make_callback_data(f"aca|{data[1]}|{data[2]}"),
                    )
                ],
                [
                    InlineKeyboardButton(
                        "Gegenargument verwerfen",
                        callback_data=make_callback_data("daca"),
                    )
                ],
            ]
        )
        client.send_message(
            chat_id=config["other"]["chat_id"],
            text=f"```{data[1]}``` als Gegenargument gegen ```{data[2]}``` hinzufügen?",
            reply_markup=keyboard,
        )
    # add counter argument
    elif data[0] == "aca":
        argument = Argument.get(Argument.argument == data[2])
        Counterargument.create(counterargument=data[1], argument=argument)
        callback_query.message.edit(
            f"Gegenargument {data[1]} gegen {data[2]} hinzugefügt"
        )
        callback_query.answer("Dankeschön, das Gegenargument wurde zur db hinzugefügt.")
    # don't add counter argument
    elif data[0] == "daca":
        callback_query.message.delete()
        callback_query.answer("Gegenargument wurde nicht zur Datenbank hinzugefügt")


@bot.on_inline_query()
def inline_query(client, inline_query):
    results = []
    arguments = Argument.select()
    for argument in arguments:
        if inline_query.query.lower() in argument.argument.lower():
            counterargument = choice(
                Counterargument.select().where(Counterargument.argument == argument)
            ).counterargument

            results.append(
                InlineQueryResultArticle(
                    title=argument.argument,
                    input_message_content=InputTextMessageContent(counterargument),
                    description=counterargument,
                )
            )
    inline_query.answer(results=results, cache_time=1)


bot.run()
